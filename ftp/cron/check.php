<?php

function is_dir_empty($dir) {
  if (!is_readable($dir)) return NULL; 
  return (count(scandir($dir)) == 2);
}

//check if users modified
$file = "/etc/vsftpd/virtual_users.txt";
$modifiedTime = filemtime($file);
//if (time() - $modifiedTime <= 60)
	//exit(1);

shell_exec("echo '' > /tmp/cron.log");

//ftp users were modified
$content = file_get_contents($file);
$lines = explode("\n", $content);

$lines = array_filter($lines, function($element) {
	return !empty($element);
});
$userNames = array_filter(
    $lines,
    function ($key) {
        return $key === 0 || $key % 2 === 0;
    },
    ARRAY_FILTER_USE_KEY
);

$ftpUsersRoot = "/home/vsftpd";

//delete folders that do not match any user name
$userfolders = scandir($ftpUsersRoot);
unset($userfolders[0]);
unset($userfolders[1]);	
foreach ($userfolders as $key => &$folder) 
{
	if (!in_array($folder, $userNames))
	{
		shell_exec("umount $ftpUsersRoot/$folder");
		rmdir("$ftpUsersRoot/$folder");
	}
}

//create folders for present users and mount project dir
foreach ($userNames as $key => $name) 
{
	$dir = "$ftpUsersRoot/$name";

	if (!file_exists($dir))
		mkdir($dir, 0755);

	if (is_dir_empty($dir))
	{
		//mount bind project
		if (strpos($name, ".sinc") !== FALSE)
			//demo
			shell_exec("mount --bind /home/developer/demo/$name /home/vsftpd/$name");
		else 
			//prod
			shell_exec("mount --bind /home/developer/prod/$name /home/vsftpd/$name");
	}
}

shell_exec("/usr/bin/db_load -T -t hash -f /etc/vsftpd/virtual_users.txt /etc/vsftpd/virtual_users.db");

echo PHP_EOL.date("D M j G:i:s T Y").' - Ftp users updated.'.PHP_EOL.'================================================='.PHP_EOL;