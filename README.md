Never delete empty-docker-compose.yml! 
Its presence is necessary to build relative paths due to the following issue:
https://github.com/docker/compose/issues/3874
